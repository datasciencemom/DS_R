#################
# MC_OilInPlace.R
# Monte Carlo simulation for oil in place
# 
# http://petrowiki.org/Monte_Carlo_simulation
# http://www.statvision.com/webinars/Monte%20Carlo%20Simulation.pdf
# 
# Author: Yang Cong
# Created Date: 7/7/2018
# Modified Date: 7/8/2018
# Run Date: 7/8/2018
################

options(scipen=999)
library(ggplot2)
library(truncnorm)
#library(scales)

# given mean and sd of X, calculate meanlog and sdlog for mean and sd of log(X)   
log_dist <- function(m, sd){
        #https://stats.stackexchange.com/questions/95498/how-to-calculate-log-normal-parameters-using-the-mean-and-std-of-the-given-distr
        meanlog <- log(m) - log(1+(sd/m)^2)/2
        sdlog <- sqrt(log(1 + (sd/m)^2))
        return(list(meanlog = meanlog, sdlog = sdlog))
}

# generate simulation samples
get_sample <- function(n_simu, no_sample){
        # n_simu <- number of simulations
        # no_sample <- pull no_samples at a time
        
        # output df_MC #
        df_MC <- data.frame(matrix(nrow = 0, ncol = 5))
        names(df_MC) <- c("Area", "NetPay", "Porosity", "WaterSaturation", "FormationVolumeFactor")
        
        # mean(log(X)), sd(log(X)) #
        A_logDist <- log_dist(788, 906)
        H_logDist <- log_dist(250, 227)

        if (no_sample == n_simu){ # pull all the samples at once 
                
                # Area A #
                A <- rlnorm(no_sample, meanlog = A_logDist$meanlog, sdlog = A_logDist$sdlog)
                # net pay h #
                H <- rlnorm(no_sample, meanlog = H_logDist$meanlog, sdlog = H_logDist$sdlog)
                # Porosity % phi #
                phi <- rtruncnorm(no_sample, a = 0, b = 100, mean = 29.8, sd = 5.36)
                # Water saturation % Sw #
                Sw <- rgamma(no_sample, shape = 21.56, scale = 0.74)
                # Formation volume factor B0 #
                B0 <- rbeta(no_sample, shape1 = 1.14, shape2 = 9.04) + 1
                
                df_MC <- data.frame(Area = A, 
                                    NetPay = H, 
                                    Porosity = phi, 
                                    WaterSaturation = Sw, 
                                    FormationVolumeFactor = B0)
                
        } else if (no_sample == 1){ # pull one sample at a time
                
                for (i in 1:n_simu){
                        # Area A #
                        A <- rlnorm(no_sample, meanlog = A_logDist$meanlog, sdlog = A_logDist$sdlog)
                        # net pay h #
                        H <- rlnorm(no_sample, meanlog = H_logDist$meanlog, sdlog = H_logDist$sdlog)
                        # Porosity % phi #
                        phi <- rtruncnorm(no_sample, a = 0, b = 100, mean = 29.8, sd = 5.36)
                        # Water saturation % Sw #
                        Sw <- rgamma(no_sample, shape = 21.56, scale = 0.74)
                        # Formation volume factor B0 #
                        B0 <- rbeta(no_sample, shape1 = 1.14, shape2 = 9.04) + 1
                        
                        df_MC <- rbind(df_MC, 
                                       data.frame(Area = A, 
                                            NetPay = H, 
                                            Porosity = phi, 
                                            WaterSaturation = Sw, 
                                            FormationVolumeFactor = B0)
                                       )
                }
        }
        
        df_MC$OilInPlace <- 7758 * df_MC$Area * df_MC$NetPay * df_MC$Porosity/100 * (1- df_MC$WaterSaturation/100) / df_MC$FormationVolumeFactor
        
        return(df_MC)
} 

n_simu <- 10000
#no_sample <- n_simu

# pull all samples at once
ptc <- proc.time()
MC_pullAll <- get_sample(n_simu, no_sample = n_simu)
proc.time() - ptc

# pull one samples at once
ptc <- proc.time()
MC_pullOne <- get_sample(n_simu, no_sample = 1)
proc.time() - ptc

# histogram #
ggplot(MC_pullAll, aes(x=OilInPlace)) +
        geom_histogram(colour="black", fill="white") +
        scale_x_continuous(limits = c(min(MC_pullAll$OilInPlace)-1, max(MC_pullAll$OilInPlace)+1)) +
        scale_x_log10() 

ggplot(MC_pullOne, aes(x=OilInPlace)) +
        geom_histogram(colour="black", fill="white") +
        scale_x_continuous(limits = c(min(MC_pullAll$OilInPlace)-1, max(MC_pullAll$OilInPlace)+1)) +
        scale_x_log10()

# CDF #
ggplot(MC_pullAll, aes(x=OilInPlace)) + 
        stat_ecdf(geom = "step", pad = FALSE) +
        scale_x_log10()

ggplot(MC_pullOne, aes(x=OilInPlace)) + 
        stat_ecdf(geom = "step", pad = FALSE) +
        scale_x_log10()

## sensitivity analysis ##

df_cor <- data.frame(matrix(nrow=0, ncol=2))
names(df_cor) <- c('Variable', 'Correlation_Coefficient')
for (item in setdiff(names(MC_pullOne), "OilInPlace")){
        # p <- ggplot(MC_pullOne, aes_string(x= item, y = "OilInPlace")) +
        #         geom_point(shape=1) +
        #         geom_smooth(method=lm, se=FALSE) +
        #         ggtitle(paste0(item, ' vs OilInPlace'))
        # print(p)
        df_cor <- rbind(df_cor,
                        data.frame(Variable = item,
                                   Correlation_Coefficient = cor(MC_pullOne[,item], MC_pullOne$OilInPlace)))
}

ggplot(df_cor, aes(x=Variable, y=Correlation_Coefficient, label=round(df_cor$Correlation_Coefficient, 2))) +
        geom_bar(stat='identity', fill = 'lightblue') +
        geom_text(size = 3, position = position_stack(vjust = 0.8)) +
        coord_flip() +
        ggtitle('Sensitivity Chart')


# ggplot() + aes(A) + 
#         geom_histogram(aes(y=..density..), colour="black", fill="white") +
#         scale_x_continuous(limits = c(0, 6*10^3)) +
#         scale_y_continuous(limits = c(0, 15 * 10^-4))
